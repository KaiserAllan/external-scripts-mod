package net.minecraft.src;

import java.io.File;
import java.io.IOException;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;

public class ESM
{
	// File Locations
	private File scriptsFolder = new File(Minecraft.getMinecraft().mcDataDir, "Scripts");
	private String categories[] = {"Blocks", "Entities", "Items", "Recipes"};

	// Arrays To Hold Scripts In Memory
	private Block ESMBlocks[] = new Block[512];
	private Entity ESMEntities[] = new Entity[512];
	private Item ESMItems[] = new Item[512];

	// Counters
	private int ESMBlockCount   = 0,
				ESMEntityCount  = 0,
				ESMItemCount    = 0,
				ESMRecipeCount  = 0;

	// Main Method
	public void loadScripts() throws IOException
	{
		// Check For Main Folder
		if(!scriptsFolder.exists())
		{
			scriptsFolder.mkdirs();
		}

		// Iterate Over Each Script Category
		for(String category : categories)
		{
			// Check For Category Folder
			File categoryFolder = new File(scriptsFolder, category);
			File[] categoryScripts = categoryFolder.listFiles();

			if(!categoryFolder.exists())
			{
				categoryFolder.mkdirs();
			}

			// Load Actual Scripts
			for(File script : categoryScripts)
			{
				if(script.getName().endsWith(".script"))
				{
					// Blocks
					if(category == categories[0]) {
						ESMBlockCount++;
					}

					// Entities
					if(category == categories[1]) {
						ESMEntityCount++;
					}

					// Items
					if(category == categories[2]) {
						ESMItemCount++;
					}

					// Recipes
					if(category == categories[3]){
						ESMRecipeCount++;
					}
				}
			}

			System.out.println("[ESM] Loaded " + ESMBlockCount + " Blocks");
			System.out.println("[ESM] Loaded " + ESMEntityCount + " Entities");
			System.out.println("[ESM] Loaded " + ESMItemCount + " Items");
			System.out.println("[ESM] Loaded " + ESMRecipeCount + " Recipes");
		}
	}
}